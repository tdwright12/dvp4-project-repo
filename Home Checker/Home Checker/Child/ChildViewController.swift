//
//  ChildViewController.swift
//  Home Checker
//
//  Created by Trae Wright on 1/22/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications

class ChildViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var currentUserID: String!
    var nfcSession = nfcHelper()
    var nfcCode: String!
    var currentChild: ChildData!
    var ref: DatabaseReference!
    @IBOutlet weak var curfewLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    ///Sets a listener to update the values of the currentChild whenever the data base is altered and schedules a notiffication to alert the child about their upcoming check in
    override func viewDidLoad() {
        super.viewDidLoad()
        if let currentUser = Auth.auth().currentUser{
            let childCode = currentUser.phoneNumber
            let ref = Database.database().reference(withPath: "users")
            ref.observe(.value, with: { snapshot in
                let data = snapshot.value as! [String: [String: AnyObject]]
                for account in data{
                    let dataChild = account.value["Children"] as! [[String: AnyObject]]
                    for child in dataChild {
                        if child["Phone Number"] as? String == childCode!{
                            self.currentUserID = account.key
                            self.nfcCode = account.value["NFC Code"] as! String
                            self.currentChild = ChildData(name: child["Name"] as! String, curfew: child["Curfew"] as! String, ID: child["ID"] as! Int, number: child["Phone Number"] as! String)
                            self.currentChild.todayCheckIn = child["Todays Check In"] as? String
                            if let checkData = child["Past Check Ins"] as? [[String: AnyObject]]{
                                for Cdat in checkData{
                                    self.currentChild.pastCheckIns.append(CheckInData(checkIn: Cdat["Check In Date"] as! String, curfew: Cdat["Curfew"] as! String))
                                }
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                self.curfewLbl.text = "Curfew: " + (self.currentChild.curfew ?? "No curfew set")
                self.scheduleNotification()
            })
        }
    }
    
    ///Schedule a LOCAL notifiction to go off 10 minutes before the childs curfew
    func scheduleNotification(){
        if currentChild.curfew != nil{
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                if settings.authorizationStatus == .authorized{
                    let content = UNMutableNotificationContent()
                    content.title = "Curfew"
                    content.body = "Your curfew is coming up, make sure to Check In."
                    let formatter = DateFormatter()
                    formatter.dateStyle = DateFormatter.Style.none
                    formatter.timeStyle = DateFormatter.Style.short
                    let tempS = formatter.date(from: self.currentChild.curfew)
                    formatter.dateFormat = "HH"
                    let curfewHour = formatter.string(from: tempS!)
                    formatter.dateFormat = "mm"
                    let curfewMin = formatter.string(from: tempS!)
                    var dateC = DateComponents()
                    dateC.hour = Int(curfewHour)
                    dateC.minute = Int(curfewMin)! - 10
                    let trigger = UNCalendarNotificationTrigger(dateMatching: dateC, repeats: false)
                    let notificationRequest = UNNotificationRequest(identifier: "curfewNotif", content: content, trigger: trigger)
                    UNUserNotificationCenter.current().add(notificationRequest, withCompletionHandler: { (error) in
                        if error != nil{
                            print(error!)
                        }else{
                            print("succ")
                        }
                    })
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentChild == nil{
            return 0
        }
        return currentChild.pastCheckIns.count
    }
    
    ///Updates the tbale view with the childs past check in data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_ID1", for: indexPath)
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let checkDate = formatter.date(from: currentChild.pastCheckIns[indexPath.row].checkInDate)
        formatter.dateFormat = "MM-dd-yyyy"
        let currentDateString = formatter.string(from: Date())
        formatter.dateStyle = DateFormatter.Style.none
        formatter.timeStyle = DateFormatter.Style.short
        let checkDateString = formatter.string(from: checkDate!)
        cell.detailTextLabel?.text = "Curfew: " + currentChild.pastCheckIns[indexPath.row].curfew + "  Check in time: " + checkDateString
        cell.textLabel?.text = "Date: \(currentDateString)"
        return cell
    }
    
    ///Updates the value of the firebase check in data for the child is the nfc scan was succsesful
    func onNFCResult(success: Bool, msg: String) {
        DispatchQueue.main.async {
            if success{
                if self.nfcCode == msg{
                    let ref = Database.database().reference(withPath: "users")
                    let userRef = ref.child(self.currentUserID)
                    let childrenRef = userRef.child("Children")
                    let childRef = childrenRef.child(self.currentChild.ID.description)
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let tString = formatter.string(from: Date())
                    if self.currentChild.todayCheckIn != nil{//checks to see if there already a today check in registered
                        var count = 0
                        childRef.updateChildValues([
                            "Todays Check In": tString
                            ])
                        if self.currentChild.pastCheckIns.count > 0{
                            count = self.currentChild.pastCheckIns.count
                            let dataRef = childRef.child("Past Check Ins")
                            dataRef.updateChildValues([
                                count.description: [
                                    "Curfew": self.currentChild.curfew,
                                    "Check In Date": self.currentChild.todayCheckIn
                                ]]
                            )
                            self.currentChild.todayCheckIn = tString
                        }else{
                            childRef.updateChildValues([
                                "Past Check Ins": [count.description: [
                                    "Curfew": self.currentChild.curfew,
                                    "Check In Date": self.currentChild.todayCheckIn
                                    ]]])
                        }
                    }else{
                        self.currentChild.todayCheckIn = tString
                        childRef.updateChildValues([
                            "Todays Check In": self.currentChild.todayCheckIn
                            ])
                    }
                    
                }
            }
        }
    }
    
    ///Signs out the users and then sets the vc to the inital vc
    @IBAction func signOutBtn(_ sender: Any) {
        do{
            try Auth.auth().signOut()
            performSegue(withIdentifier: "signOut", sender: nil)
        }catch{
            print("Error signing out")
        }
    }
    
    ///Starts the nfc session using the NFCHelper class
    @IBAction func checkInBtn(_ sender: Any) {
        nfcSession.onNFCResult = onNFCResult(success:msg:)
        nfcSession.startSession()
    }
}
