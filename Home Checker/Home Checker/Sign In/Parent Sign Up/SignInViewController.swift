//
//  SignInViewController.swift
//  Home Checker
//
//  Created by Trae Wright on 1/15/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit
import Firebase

class SignInViewController: UIViewController {
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    ///Ends editing when the user touches out side of a keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //checks to see if there is an account with the information the user entered and if there is not one then a error pop up shown telling the user its wrong
    @IBAction func signInBtnPress(_ sender: Any) {
        Auth.auth().signIn(withEmail: emailTxt.text!, password: passwordTxt.text!) { (user, error) in
            if error == nil {
                self.performSegue(withIdentifier: "signIn", sender: user?.uid)
            }else{
                let alert = UIAlertController(title: "Error",
                                              message: "Your email or password is invalid, please try again.",
                                              preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok",
                                             style: .default)
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationNC = segue.destination as? UINavigationController {
            if let destinationVC = destinationNC.viewControllers.first as? FirstViewController{
                destinationVC.currentUserID = sender as! String
            }
        }
    }
}
