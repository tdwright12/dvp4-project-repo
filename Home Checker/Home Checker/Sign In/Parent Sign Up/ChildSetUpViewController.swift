//
//  ChildSetUpViewController.swift
//  Home Checker
//
//  Created by Trae Wright on 1/15/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit
import Firebase

class ChildSetUpViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var tableViewOut: UITableView!
    var newParent: ParentData!
    var nfcAddBtnTag: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newParent.childList.count
    }
    
    ///Updates the table view with the list of new children created
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell_Id1", for: indexPath) as? CustomChildTableViewCell{
            let child = newParent.childList[indexPath.row]
            cell.childNameLbl.text = child.name
            cell.curfewLbl.text = "Phone Number: "+child.phoneNumber.description
            return cell
        }else{
            return tableView.dequeueReusableCell(withIdentifier: "cell_Id1", for: indexPath)
        }
    }
    
    ///unwind from NewChildVC getting the newChild data created there
    @IBAction func unwindToSignUp(segue:UIStoryboardSegue) {
        if let source = segue.source as? NewChildViewController{
            if source.newChild != nil{
                newParent.childList.append(source.newChild)
                let offset = newParent.childList.count-1
                newParent.childList[offset].ID = offset
                self.doneBtn.setTitle("Done", for: .normal)
                tableViewOut.reloadData()
            }
        }
    }
    
    ///Pass the data required for the next VCs
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selecetedNC = segue.destination as? UINavigationController{
            if let selecetedVC = selecetedNC.viewControllers.first as? FirstViewController{
                selecetedVC.currentUserID = newParent.ID
                let ref = Database.database().reference(withPath: "users")
                let pref = ref.child(newParent.ID)
                pref.setValue(newParent.toAnyObject())//Adds the current parent objec to the firebase database
            }
        }else if let destinationVC = segue.destination as? NewChildViewController{
            destinationVC.signUpSource = true
            destinationVC.newChild = ChildData(ID: newParent.childList.count)
        }
    }
}
