//
//  NewChildViewController.swift
//  Home Checker
//
//  Created by Trae Wright on 1/16/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit

class NewChildViewController: UIViewController {
    
    @IBOutlet weak var numberTxt: UITextField!
    @IBOutlet weak var curfewTxt: UITextField!
    @IBOutlet weak var nameTxt: UITextField!
    var newChild: ChildData!
    var signUpSource = false
    var curfewTimeInter = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    ///Ends editing when the screen is touched outside of the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    ///Shows a date picker instead of a keyboard
    @IBAction func textFieldEditing(sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    
    ///action for the date picker created in textFieldEditing. Puts the formatted date into the text field the was selected in the date picker
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.none
        dateFormatter.timeStyle = DateFormatter.Style.short
        curfewTimeInter = dateFormatter.string(from: sender.date)
        curfewTxt.text = curfewTimeInter
    }
    
    ///preforms the segue back tothe view controller it came from
    @IBAction func cancelBtn(_ sender: Any) {
        if signUpSource{// this value is set to true only when this view controller id created from signUPVc
            performSegue(withIdentifier: "toSignUp", sender: self)
        }else{
            performSegue(withIdentifier: "toParent", sender: self)
        }
    }
    
    ///Saves the newChild then preforms the unwind segue back to the VC it came from
    @IBAction func saveBtnPress(_ sender: Any) {
        if numberTxt.text!.isEmpty, curfewTxt.text!.isEmpty, nameTxt.text!.isEmpty{
            let alert = UIAlertController(title: "Error",
                                          message: "Please do not leave any of the fields blank.",
                                          preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok",
                                         style: .default)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
        }else{
            newChild = ChildData(name: nameTxt.text!, curfew: curfewTimeInter, ID: 0, number: "+1"+numberTxt.text!)
            if signUpSource{
                performSegue(withIdentifier: "toSignUp", sender: self)
            }else{
                performSegue(withIdentifier: "toParent", sender: self)
            }
        }
    }
}
