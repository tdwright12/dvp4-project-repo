//
//  CustomChildTableViewCell.swift
//  Home Checker
//
//  Created by Trae Wright on 1/16/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit

class CustomChildTableViewCell: UITableViewCell {
    @IBOutlet weak var childNameLbl: UILabel!
    @IBOutlet weak var curfewLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
