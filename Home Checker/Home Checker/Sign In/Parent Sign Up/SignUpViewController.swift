//
//  SignUpViewController.swift
//  Home Checker
//
//  Created by Trae Wright on 1/15/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController {
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var passwordConfirmTxt: UITextField!
    var newParent: ParentData!
    var nfcCode: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    ///takes what the user entered and adds it to create a parent data to be passed on to the next VC
    @IBAction func signUpBtnPress(_ sender: Any) {
        let email = emailTxt.text!
        let password = passwordTxt.text!
        if passwordTxt.text == passwordConfirmTxt.text && passwordTxt.text != ""{
            Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
                if error == nil {
                    self.newParent = ParentData.init(name: self.nameTxt.text!, childList: [], ID: (user?.uid)!, email: email, password: password, nfcCode: self.nfcCode)
                    Auth.auth().signIn(withEmail: self.emailTxt.text!,
                                       password: self.passwordTxt.text!)
                    //Auth.auth().currentUser?.sendEmailVerification {(error) in}
                    self.performSegue(withIdentifier: "signUp", sender: nil)
                }
            }
        }else{
            let alert = UIAlertController(title: "Error",
                                          message: "Your passwords do not match or they are blank.",
                                          preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok",
                                             style: .default)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
        }
    }
    
    ///Passes off the newParent created in the signUpBtnPress @IBAction
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ChildSetUpViewController {
            destinationVC.newParent = newParent
        }
    }
}
