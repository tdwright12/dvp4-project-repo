//
//  PorCViewController.swift
//  Home Checker
//
//  Created by Trae Wright on 1/18/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit
import Firebase

class PorCViewController: UIViewController {
    var nfcCode: String!
    var childList: [ChildData] = []
    var parentID: String!
    @IBOutlet weak var parentBtn: UIButton!
    @IBOutlet weak var childBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        parentBtn.layer.cornerRadius = 15
        childBtn.layer.cornerRadius = 15
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ///Gets the child data for the upcoming screen "NumberSelection" based on the NFC code scaned
    @IBAction func childSignInTap(_ sender: Any) {
        let ref = Database.database().reference(withPath: "users")
        ref.observe(.value, with: { snapshot in
            let data = snapshot.value as! [String: [String: AnyObject]]
            for account in data{
                if account.value["NFC Code"] as? String == self.nfcCode{
                    let dataChild = account.value["Children"] as! [[String: AnyObject]]
                    if dataChild.count > 0{
                        for child in dataChild {
                            self.childList.append(ChildData(name: child["Name"] as! String, curfew: child["Curfew"] as! String, ID: child["ID"] as! Int, number: child["Phone Number"] as! String))
                        }
                        self.parentID = account.key
                        self.performSegue(withIdentifier: "toChildSignIn", sender: nil)
                    }else{//For when there are no children in side of the account
                        let alert = UIAlertController(title: "Error",
                                                      message: "There are no children linked to your NFC box. Please ask your parent to add you under their account.",
                                                      preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "Ok",
                                                     style: .default)
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        })
    }
    
    //Checks to see if there is an account that matches the NFC code that was scanned and if one is found then it displays a warming message telling user to sign in
    @IBAction func parentBtn(_ sender: Any) {
        let ref = Database.database().reference(withPath: "users")
        ref.observe(.value, with: { snapshot in
            let data = snapshot.value as! [String: [String: AnyObject]]
            var foundNFC = false
            for account in data{
                if account.value["NFC Code"] as? String == self.nfcCode{
                    foundNFC = true
                }
            }
            if foundNFC == true{
                let alert = UIAlertController(title: "Error",
                                              message: "There is already a parent account connected to the scanned NFC tag, please sign in",
                                              preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok",
                                             style: .default)
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }else{
                self.performSegue(withIdentifier: "toParentSignIn", sender: nil)
            }
        })
    }
    
    ///Sends over the nessecary data for the next screens
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? SignUpViewController{
            destination.nfcCode = nfcCode
        }else if let navControl = segue.destination as? UINavigationController{
            if let destination = navControl.viewControllers.first as? NumberSelectionTableView{
                destination.childList = childList
                destination.parentID = parentID
            }
        }
    }
}
