//
//  NFCscanViewController.swift
//  Home Checker
//
//  Created by Trae Wright on 1/18/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit
import Firebase

class NFCscanViewController: UIViewController {
    @IBOutlet weak var nfcBtn: UIButton!
    var userID: String = ""
    var nfcCode: String!
    var nfcSession = nfcHelper()
    
    ///Sets the nfcBtn's shape and then attemps to auto-sign in a user if one is signed in
    override func viewDidLoad() {
        super.viewDidLoad()
        nfcBtn.titleLabel?.numberOfLines = 2
        nfcBtn.layer.cornerRadius = 100
        nfcBtn.titleLabel?.textAlignment = NSTextAlignment.center
//        if let currentUser = Auth.auth().currentUser{
//            userID = currentUser.uid
//            if currentUser.phoneNumber != nil{
//                DispatchQueue.main.async {//Check this feature.. Temporary fix
//                    self.performSegue(withIdentifier: "child", sender: nil)
//                }
//            }else{
//                DispatchQueue.main.async {//Check this feature.. Temporary fix
//                    self.performSegue(withIdentifier: "parent", sender: nil)
//                }
//            }
//        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selecetedNC = segue.destination as? UINavigationController{
            if let destinationVC = selecetedNC.viewControllers.first as? FirstViewController{
                destinationVC.currentUserID = userID
            }
        }else if let destinationVC = segue.destination as? PorCViewController{
            destinationVC.nfcCode = nfcCode
        }
    }
    
    ///If the NFC session is a success full then the nfc code is saved and the segue to sign in is performed
    func onNFCResult(success: Bool, msg: String) {
        DispatchQueue.main.async {
            if success{
                self.nfcCode = msg
                self.performSegue(withIdentifier: "toSignIn", sender: nil)
            }
            
        }
    }
    
    ///Easy way to sign out from any screen in the application
    @IBAction func unwindSignOut(segue:UIStoryboardSegue) {}
    
    ///Starts the nfc session inside of nfcHelper
    @IBAction func setNFCBtnPress(_ sender: Any) {
        nfcSession.onNFCResult = onNFCResult(success:msg:)
        nfcSession.startSession()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
