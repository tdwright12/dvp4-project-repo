//
//  PhoneVerifyViewController.swift
//  Home Checker
//
//  Created by Trae Wright on 1/19/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit
import WebKit
import FirebaseAuth

class PhoneVerifyViewController: UIViewController {
    var parentID: String!
    var selectedChild: ChildData!
    var verificationCode: String!
    var verfyID: String!
    @IBOutlet weak var phoneNumberTxt: UILabel!
    @IBOutlet weak var verificationCodeTxt: UITextField!
    
    ///Sends the verification code to the phone number provided by the child's parent and saves the verification id to sign in when the code is entered
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberTxt.text = "Please enter the verification code that was sent to \(selectedChild.phoneNumber)"
        PhoneAuthProvider.provider().verifyPhoneNumber(selectedChild.phoneNumber, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print(error)
                return
            }
            self.verfyID = verificationID!
        }
    }
    
    ///Attemps to sign the user in using the verification code and the verfication ID If any info is invalid then a alert is shown
    func signInUser() {
        if verificationCode != nil, verfyID != nil{
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: verfyID,
                verificationCode: self.verificationCode)
            Auth.auth().signIn(with: credential) { (user, error) in
                if error != nil {
                    let alert = UIAlertController(title: "Error",
                                                  message: "The verification code entered is invalid.",
                                                  preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok",
                                                 style: .default)
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                    return
                }
                self.performSegue(withIdentifier: "signInSuccsess", sender: nil)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    ///Doesnt allow the user to enter more than 6 characters when the uerr types in 6 characters then it attemps to sign in to the account using the cude entered
    @IBAction func editChange(_ sender: UITextField) {
        let number = sender.text!
        if number.count > 6{
            sender.deleteBackward()
        }else if number.count == 6{
            verificationCode = number
            signInUser()
        }
    }
    
    ///Re sends the verification code to the users phone
    @IBAction func reSendBtn(_ sender: Any) {
        PhoneAuthProvider.provider().verifyPhoneNumber(selectedChild.phoneNumber, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            self.verfyID = verificationID!
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ChildViewController{
            destination.currentUserID = parentID
        }
    }
}
