//
//  NumberSelectionTableView.swift
//  Home Checker
//
//  Created by Trae Wright on 1/19/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit

class NumberSelectionTableView: UITableViewController {
    var childList: [ChildData] = []
    var parentID: String!
    var selectedChild: ChildData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return childList.count
    }

    ///Loads the childs names and phine numbers into the table view's cells
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseId_1", for: indexPath)
        cell.textLabel?.text = childList[indexPath.row].name
        cell.detailTextLabel?.text = "\(childList[indexPath.row].phoneNumber)"
        return cell
    }
    
    ///Sets the selected child based on the row selected then perfoms the segue to the Verify controller
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedChild = childList[indexPath.row]
        performSegue(withIdentifier: "toVerify", sender: nil)
    }

    ///Sends the parentId and selectedChild data to the new VC
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? PhoneVerifyViewController{
            destination.selectedChild = selectedChild
            destination.parentID = parentID
        }
    }
}
