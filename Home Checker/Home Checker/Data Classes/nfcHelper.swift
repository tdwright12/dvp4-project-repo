//
//  nfcHelper.swift
//  Home Checker
//
//  Created by Trae Wright on 1/18/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import Foundation
import CoreNFC

class nfcHelper: NSObject, NFCNDEFReaderSessionDelegate {
    var nfcCode: String!
    var onNFCResult: ((Bool, String) -> ())?
    
    ///Starts the NFC session setting the delegate to this swift file
    func startSession(){
        let nfcSession = NFCNDEFReaderSession.init(delegate: self, queue: nil, invalidateAfterFirstRead: true)
        nfcSession.begin()
    }
    
    ///Prints the error if there is a problem reading the NFC tag
    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
        print(error)
    }
    
    ///gets the payload message form the scanned NFC tag and the returns the result through onNFCResult
    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        guard let onNFCResult = onNFCResult else {return}
        var result = ""
        for payload in messages[0].records {
            result += String.init(data: payload.payload.advanced(by: 3), encoding: .utf8)! // 1
        }
        onNFCResult(true, result)
    }
}
