//
//  CheckInData.swift
//  Home Checker
//
//  Created by Trae Wright on 1/21/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import Foundation

class CheckInData{
    var checkInDate: String
    var curfew: String
    
    init(checkIn: String, curfew: String) {
        checkInDate = checkIn
        self.curfew = curfew
        
    }
}

