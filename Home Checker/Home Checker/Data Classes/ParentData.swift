//
//  ParentData.swift
//  Home Checker
//
//  Created by Trae Wright on 1/11/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import Foundation
import Firebase

struct ParentData {
    var name: String
    var childList: [ChildData]
    var ID: String
    var email: String
    var password: String
    var nfcCode: String!
    
    init(name: String, childList: [ChildData], ID: String, email: String, password: String, nfcCode: String!) {
        self.name = name
        self.childList = childList
        self.ID = ID
        self.email = email
        self.password = password
        self.nfcCode = nfcCode
    }
    
    ///takes in a snapshot of a dictionary from the database and then converts it into a parent object
    init(snapshot: [String: AnyObject], ID: String) {
        self.ID = ID
        name = snapshot["Name"] as! String
        email = snapshot["Email"] as! String
        password = snapshot["Password"] as! String
        nfcCode = snapshot["NFC Code"] as! String!
        if let children = snapshot["Children"] as? [[String: AnyObject]]{
            childList = []
            for child in children {
                guard let childID = child["ID"] as? Int, let childName = child["Name"] as? String, let curfew = child["Curfew"] as? String, let phoneNumber = child["Phone Number"] as? String else{continue}
                let tempChild: ChildData = ChildData(name: childName, curfew: curfew, ID: childID, number: phoneNumber)
                if let checkInTime = child["Todays Check In"] as? String{
                    tempChild.todayCheckIn = checkInTime
                }
                childList.append(tempChild)
            }
        }else{
            childList = []
        }
    }
    
    ///converts this parent object and their children into an any object that contains a dictionary for firebase
    func toAnyObject() -> Any {
        var arrayOfChildren: [[String: AnyObject]] = []
        if childList.count > 0{
            for child in childList{//cycles through each child the saves their info into an array of dictionarys to later be converted to an anyobject below
                var childDict: [String: AnyObject] = [:]
                childDict.updateValue(child.ID as AnyObject, forKey: "ID")
                childDict.updateValue(child.name as AnyObject, forKey: "Name")
                childDict.updateValue(child.curfew as AnyObject, forKey: "Curfew")
                childDict.updateValue(child.phoneNumber as AnyObject, forKey: "Phone Number")
                if child.todayCheckIn != nil{
                    childDict.updateValue(child.todayCheckIn as AnyObject, forKey: "Todays Check In")
                }
                arrayOfChildren.append(childDict)
            }
        }
        return [
            "Name": name,
            "Email": email,
            "Password": password,
            "NFC Code": nfcCode,
            "Children": arrayOfChildren
        ]
    }
}
