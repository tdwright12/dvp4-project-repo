//
//  ChildData.swift
//  Home Checker
//
//  Created by Trae Wright on 1/11/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import Foundation

class ChildData {
    var name: String
    var curfew: String!
    var ID: Int
    var phoneNumber: String
    var todayCheckIn: String!
    var pastCheckIns: [CheckInData] = []
    
    init(name: String, curfew: String!, ID: Int, number: String) {
        self.name = name
        self.curfew = curfew
        self.ID = ID
        self.phoneNumber = number
    }
    
    convenience init(name: String, number: String, ID: Int){
        self.init(name: name, curfew: nil, ID: ID, number: number)
    }
    
    convenience init(ID: Int){
        self.init(name: "", curfew: nil, ID: ID, number: "")
    }
    
    //Sets todays check in's value to the current date in string format
    func dateToCheckIn(){
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if todayCheckIn != nil{//if there is already a checkin today then it is appened to the checkindata array before being saved over
            pastCheckIns.insert(CheckInData(checkIn: todayCheckIn, curfew: curfew), at: 0)
        }
        todayCheckIn = formatter.string(from: Date())
    }
}
