//
//  CustomCollectionViewCell.swift
//  Home Checker
//
//  Created by Trae Wright on 1/22/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var homeAwayImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var curfewLbl: UILabel!
    @IBOutlet weak var awayLbl: UILabel!
}
