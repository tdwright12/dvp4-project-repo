//
//  ChildInfoViewController.swift
//  Home Checker
//
//  Created by Trae Wright on 1/24/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit
import Firebase

class ChildInfoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var parentID: String!
    var childInfo: ChildData!
    var checkInD: [CheckInData] = []
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var curfewTxt: UITextField!
    @IBOutlet weak var phoneNumberTxt: UITextField!
    @IBOutlet weak var todayCheckInTxt: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    ///Sets all of the fileds in the data base with the data from firbase and the selectedChild
    override func viewDidLoad() {
        super.viewDidLoad()
        let usersRef = Database.database().reference(withPath: "users").child(parentID).child("Children").child(childInfo.ID.description)
        usersRef.observe(.value, with: { snapshot in
            let data = snapshot.value as! [String: AnyObject]
            self.childInfo.todayCheckIn = data["Todays Check In"] as? String
            if let checkData = data["Past Check Ins"] as? [[String: AnyObject]]{
                                        var tempArray: [CheckInData] = []
                    for checkIn in checkData{
                        tempArray.append(CheckInData(checkIn: checkIn["Check In Date"] as! String, curfew: checkIn["Curfew"] as! String))
                    }
                self.checkInD = tempArray
                self.tableView.reloadData()
                if self.childInfo.todayCheckIn != nil{//Checks to see if there has been a check in today
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let checkDate = formatter.date(from: self.childInfo.todayCheckIn)
                    formatter.dateStyle = DateFormatter.Style.none
                    formatter.timeStyle = DateFormatter.Style.short
                    let checkDateString = formatter.string(from: checkDate!)
                    self.todayCheckInTxt.text = checkDateString
                }else{
                    self.todayCheckInTxt.text = "No check in recorded"
                }
            }
        })
        nameTxt.text = childInfo.name
        curfewTxt.text = childInfo.curfew
        phoneNumberTxt.text = childInfo.phoneNumber
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    ///Sets the table view cell with the data from the childs past check in data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_ID1", for: indexPath)
        let formatter = DateFormatter()//formattes the dates
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let checkDate = formatter.date(from: checkInD[indexPath.row].checkInDate)
        formatter.dateFormat = "MM-dd-yyyy"
        // again convert your date to string
        let currentDateString = formatter.string(from: Date())
        formatter.dateStyle = DateFormatter.Style.none
        formatter.timeStyle = DateFormatter.Style.short
        let checkDateString = formatter.string(from: checkDate!)
        cell.detailTextLabel?.text = "Curfew: " + checkInD[indexPath.row].curfew + "  Check in time: " + checkDateString
        cell.textLabel?.text = "Date: \(currentDateString)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return checkInD.count
    }
    
    ///Closes the keyboard if the screen is touched outside of the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    ///changes the keyboard to date picker and then sets an action for the date picker
    @IBAction func texteditingBegin(_ sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    
    ///when the date changes it updates the text fields and formates the date selected
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.none
        dateFormatter.timeStyle = DateFormatter.Style.short
        childInfo.curfew = dateFormatter.string(from: sender.date)
        curfewTxt.text = childInfo.curfew
    }
    
    ///updates the child data with the data that has been altered in the view
    override func viewWillDisappear(_ animated: Bool) {
        let ref = Database.database().reference(withPath: "users")
            let userRef = ref.child(parentID)
        let childrenRef = userRef.child("Children")
        let childRef = childrenRef.child(childInfo.ID.description)
        childRef.updateChildValues(["Curfew": self.childInfo.curfew])
    }
}
