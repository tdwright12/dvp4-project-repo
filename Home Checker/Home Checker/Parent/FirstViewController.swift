//
//  FirstViewController.swift
//  Home Checker
//
//  Created by Trae Wright on 1/11/18.
//  Copyright © 2018 Trae Wright. All rights reserved.
//

import UIKit
import Firebase

class FirstViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    var ref: DatabaseReference!
    var currentParent: ParentData!
    var currentUserID: String = "error"
    var selectedIndex: Int!
    @IBOutlet weak var collectionView: UICollectionView!
    
    ///Sets a listener to moniter the data entered into the database so the UI can update
    override func viewDidLoad() {
        super.viewDidLoad()
        let logo = UIImage(named: "Banner Icon.png")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        currentParent = ParentData(name: "", childList: [], ID: "", email: "", password: "", nfcCode: nil)
        let usersRef = Database.database().reference(withPath: "users")
        ref = usersRef.child(currentUserID)
        self.ref.observe(.value, with: { snapshot in
            let parent = ParentData(snapshot: snapshot.value as! [String: AnyObject], ID: self.currentUserID)
            self.currentParent = parent
            for child in parent.childList{
                if child.todayCheckIn != nil{
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let curfewTime = formatter.date(from: child.todayCheckIn!)
                    formatter.dateFormat = "yyyy-MM-dd"
                    let dateReFormat = formatter.string(from: Date())
                    let string = formatter.string(from: curfewTime!)
                    let dRE = formatter.date(from: dateReFormat)
                    let dCheck = formatter.date(from: string)
                    if dRE! > dCheck!{//If it has been a day sense the last check in then this is set to til
                        child.todayCheckIn = nil
                    }
                }
            }
            self.collectionView.reloadData()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    ///This is an unwind from the newChildVC it takes the new child created and adds it to the currrentParent and the database
    @IBAction func unwindToParentVC(segue:UIStoryboardSegue) {
        if let source = segue.source as? NewChildViewController{
            if source.newChild != nil{
                currentParent.childList.append(source.newChild)
                let offset = currentParent.childList.count-1
                currentParent.childList[offset].ID = offset
                let newChild = currentParent.childList[offset]
                let ref = Database.database().reference(withPath: "users")
                let userRef = ref.child(self.currentUserID)
                let childrenRef = userRef.child("Children")
                childrenRef.updateChildValues([
                    newChild.ID.description: [
                        "Curfew": newChild.curfew,
                        "ID": newChild.ID,
                        "Name": newChild.name,
                        "Phone Number": newChild.phoneNumber
                    ]])
                collectionView.reloadData()
            }
        }
    }
    
    ///Signs out the current user then return to the inital VC
    @IBAction func signOutBtn(_ sender: Any) {
        do{
            try Auth.auth().signOut()
            performSegue(withIdentifier: "signOut", sender: nil)
        }catch{
            print("Error signing out")
        }
    }
    
    ///Sends the parent user Id and the child data for the Child INfo VC
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ChildInfoViewController{
            destinationVC.parentID = currentParent.ID
            destinationVC.childInfo = currentParent.childList[selectedIndex]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentParent.childList.count
    }
    
    ///Sets the custom collection view cell with data form each child in the list
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_ID1", for: indexPath) as! CustomCollectionViewCell
        let child = currentParent.childList[indexPath.row]
        cell.nameLbl.text = child.name
        cell.curfewLbl.text = "Curfew " + child.curfew
        if child.todayCheckIn == nil{//Determins if the child is home or away
            cell.homeAwayImage.image = #imageLiteral(resourceName: "away")
            cell.awayLbl.text = "Away"
        }else{
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let curfewTime = formatter.date(from: child.todayCheckIn!)
            formatter.dateStyle = DateFormatter.Style.none
            formatter.timeStyle = DateFormatter.Style.short
            let string = formatter.string(from: curfewTime!)
            cell.awayLbl.text = "Home at \(string)"
            cell.homeAwayImage.image = #imageLiteral(resourceName: "home")
        }
        return cell
    }
    
    ///Sets the selected index to the index path selected in the collection view and then performs the segue to view childVC
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "viewChild", sender: nil)
    }
}

