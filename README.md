# README #

### Git Repository Link ###

* https://bitbucket.org/tdwright12/dvp4-project-repo

### Final Feature List ###

* Reomte database for user accounts with ability to add child accounts to the main account.
* Ability to read NFC tag for the children.
* Keeps track of the date and time for each kid's check ins and shows it visualy.
* Sends a push notification to the child's phone to let them know if they are close to running late.
* Lock the child account out of all of the parental features of the application(Add/Remove child, maybe statistics of check ins?).
* Saves the exact time that the child checked for later viewing by the parent.
* Synchronize the applications local data with a remote data base hosted by fire base.
* A quick view feature on the home screen to show whos home.
* Set the curfew for a child of their choosing.
* Allow the child to see what curfew is set for them.


### Installation instructions ###

* Go to downloads
* Click download repository
* Open the home checker file inside of the downloaded zip
* Then run the file named "Home Checker.xcworkspace"(Do not run "Home Checker.xcodeproj")

### Coco Pods ###

* pod 'Firebase/Core'
* pod 'Firebase/Database'
* pod 'Firebase/Auth'
* pod 'Firebase/Messaging'

### Hardware considerations and requirements ###

* In order to run the application an iphone 7 or higher is required
* If you do not have am iPhone 7 or higher then sign in under the test account(this may not always work)

### Test Account Login information ###

* Email- test@account.com
* Password- password

### Contact Information ###

* My name is Trae Wright and i currently studing mobile development
* Email- trae.wright12@gmail.com